touch $(hostname)_$(date +%y-%m-%d)
for i in {1..100}
do
   echo $(($(date +%s%N)/1000000))'|'$((RANDOM%1000+1))'|'$(hostname)'|'$(cat /etc/os-release | grep "PRETTY_NAME" | sed 's/PRETTY_NAME=//g' | sed 's/["]//g' | awk '{print $1}')'|'$(uptime -p) >> $(hostname)_$(date +%y-%m-%d)
done
aws s3 mv $(hostname)_$(date +%y-%m-%d) s3://s3chami/$(hostname)_$(date +%y-%m-%d)
sleep 30
